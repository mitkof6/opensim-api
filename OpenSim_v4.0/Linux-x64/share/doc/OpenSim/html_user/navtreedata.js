/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "API", "index.html", [
    [ "Overview", "index.html", null ],
    [ "API Guide", "md_doc_APIGuide.html", [
      [ "Intended Audience", "md_doc_APIGuide.html#intendedaudience", null ],
      [ "Overview of the OpenSim API for Developers", "md_doc_APIGuide.html#overviewoftheapi", [
        [ "OpenSim’s Purpose", "md_doc_APIGuide.html#opensimpurpose", null ],
        [ "Design Philosophy", "md_doc_APIGuide.html#designphilosophy", null ],
        [ "Organization of OpenSim", "md_doc_APIGuide.html#organization", null ],
        [ "A Simple Example", "md_doc_APIGuide.html#simpleexample", null ],
        [ "System and State", "md_doc_APIGuide.html#systemstate", null ],
        [ "Object", "md_doc_APIGuide.html#object", null ],
        [ "Component", "md_doc_APIGuide.html#component", [
          [ "Models are composed of Components", "md_doc_APIGuide.html#modelscomposed", null ],
          [ "Property", "md_doc_APIGuide.html#property", null ],
          [ "Socket", "md_doc_APIGuide.html#socket", null ],
          [ "Inputs and Outputs", "md_doc_APIGuide.html#inputsoutputs", null ],
          [ "Operator", "md_doc_APIGuide.html#operator", null ],
          [ "Source", "md_doc_APIGuide.html#source", null ],
          [ "Reporter", "md_doc_APIGuide.html#reporter", null ],
          [ "ModelComponent", "md_doc_APIGuide.html#modelcomponent", null ]
        ] ],
        [ "Principal ModelComponents", "md_doc_APIGuide.html#modelcomponents", [
          [ "Frames", "md_doc_APIGuide.html#frames", null ],
          [ "Points, Stations and Markers", "md_doc_APIGuide.html#points", null ],
          [ "Joints", "md_doc_APIGuide.html#joints", null ],
          [ "Constraints", "md_doc_APIGuide.html#constraints", null ],
          [ "Forces", "md_doc_APIGuide.html#forces", null ],
          [ "Actuators", "md_doc_APIGuide.html#actuators", null ],
          [ "Controllers", "md_doc_APIGuide.html#controller", null ]
        ] ],
        [ "Data Handling Classes", "md_doc_APIGuide.html#datahandling", null ],
        [ "Solvers", "md_doc_APIGuide.html#solvers", null ]
      ] ],
      [ "Writing your own Component", "md_doc_APIGuide.html#writingcomponents", [
        [ "I. Constructing your Component’s Attributes", "md_doc_APIGuide.html#constructproperties", null ],
        [ "II. Finalize Properties and Connections of the Component", "md_doc_APIGuide.html#finalizefromproperties", null ],
        [ "III. Adding your Component’s Dynamics to the System", "md_doc_APIGuide.html#addtosystem", null ],
        [ "IV. Initializing and Recalling the State", "md_doc_APIGuide.html#initstatefromproperties", null ],
        [ "Test Components", "md_doc_APIGuide.html#testComponents", null ]
      ] ]
    ] ],
    [ "OpenSim Copyright and License", "OpenSim_license_page.html", null ],
    [ "OpenSimProgrammingGuide", "md_doc_OpenSimProgrammingGuide.html", null ],
    [ "Bug List", "bug.html", null ],
    [ "Class Groups", "modules.html", "modules" ],
    [ "Namespace Members", "namespacemembers.html", [
      [ "All", "namespacemembers.html", null ],
      [ "Functions", "namespacemembers_func.html", null ],
      [ "Variables", "namespacemembers_vars.html", null ],
      [ "Typedefs", "namespacemembers_type.html", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List ", "annotated.html", "annotated_dup" ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Properties", "functions_prop.html", "functions_prop" ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"OpenSim_license_page.html",
"classOpenSim_1_1AbstractTool.html#ae58c0f0889d6ce71caa97974bbb3596b",
"classOpenSim_1_1Analysis.html#a5928df860efb04d303f4d0216f4507c7",
"classOpenSim_1_1Bhargava2004MuscleMetabolicsProbe.html#abe904531cff3fa9bd4b87f47a8a61cdf",
"classOpenSim_1_1CMC.html#a7c393e71023b6a5b4a7eed74d874c62c",
"classOpenSim_1_1Component.html#a2b5164b43d0264b4224723e65c5a1b5a",
"classOpenSim_1_1ConnecteeNotSpecified.html#adc971267449eb0984b712e8aad8b4418",
"classOpenSim_1_1ControlLinear.html#aa7864f0a25bedacf5ad4fae38b685e86",
"classOpenSim_1_1CoordinateLimitForce.html#a0e4cbb34869da8fb56c9bce0d6eacfd2",
"classOpenSim_1_1Delp1990Muscle__Deprecated.html#a94418739db3d9bebab6cf8a702ca4fdb",
"classOpenSim_1_1ExpressionBasedCoordinateForce.html#a57f902fce5c4038577ee2009d6e6735b",
"classOpenSim_1_1FileExtensionNotFound.html#a34bcfe55b167e944ed8ff61b68a3c18a",
"classOpenSim_1_1Frame.html#a99f988be63f2599f86a2e47384a53294",
"classOpenSim_1_1GeometryPath.html#a6502366b6a8edff365fe4c65274343dc",
"classOpenSim_1_1InputNotConnected.html#a3f262c0573d3c67ed3181b5bb25aab67",
"classOpenSim_1_1Kinematics.html#a6ea03fe4e8830b9def5db34da489f04c",
"classOpenSim_1_1MarkerWeight.html#ae70c84ec326176f2e559658a60bb07f5",
"classOpenSim_1_1Millard2012EquilibriumMuscle.html#a5f750893909c319cdeeb8271555e0269",
"classOpenSim_1_1Model.html#aca0a33c2c51988f71b407e13987c23fa",
"classOpenSim_1_1Muscle.html#a36d1dd6b10190291d02bb8491882b3fb",
"classOpenSim_1_1MuscleFixedWidthPennationModel.html#ac5193fe832bb27d81be92c0f1e0ba07f",
"classOpenSim_1_1PathPoint.html#a7695bf1a7e74190d5167303d2e1d58e2",
"classOpenSim_1_1PointForceDirection.html#ade9af6d931e2a5615e90e79674bfbe53",
"classOpenSim_1_1Probe.html#a751687c98e7a75383c439e9a507c851e",
"classOpenSim_1_1PropertyObjArray.html#a9dd160d0a0bc88b48883816823d31c3c",
"classOpenSim_1_1RRATool.html#ab5aa6c173a30afd95a00cad18e05eead",
"classOpenSim_1_1Schutte1993Muscle__Deprecated.html#ab0d8766bd296d02d1b88f5bacd919075",
"classOpenSim_1_1SpatialTransform.html#ac380b55bebc7064611049594e46bb81f",
"classOpenSim_1_1Storage.html#a5f9747f01d7b7b074b8a21dc6b038323",
"classOpenSim_1_1Thelen2003Muscle.html#a3643a71f85f4232b1b8985cbb81b5f74",
"classOpenSim_1_1TrackingTask.html#aa212776e5a808256a40a060d34ac89ac",
"classOpenSim_1_1ValueArrayDictionary.html#a884efe5b2b783d65b3a2cbc7a9e3dfe6",
"md_doc_APIGuide.html#opensimpurpose"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';