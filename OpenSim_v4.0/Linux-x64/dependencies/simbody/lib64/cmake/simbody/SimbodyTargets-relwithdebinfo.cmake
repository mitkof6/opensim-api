#----------------------------------------------------------------
# Generated CMake target import file for configuration "RelWithDebInfo".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "SimTKcommon" for configuration "RelWithDebInfo"
set_property(TARGET SimTKcommon APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(SimTKcommon PROPERTIES
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "/usr/lib/libopenblas.so;/usr/lib/liblapack.so;/usr/lib/libopenblas.so;pthread;rt;dl;m"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib64/libSimTKcommon.so.3.6"
  IMPORTED_SONAME_RELWITHDEBINFO "libSimTKcommon.so.3.6"
  )

list(APPEND _IMPORT_CHECK_TARGETS SimTKcommon )
list(APPEND _IMPORT_CHECK_FILES_FOR_SimTKcommon "${_IMPORT_PREFIX}/lib64/libSimTKcommon.so.3.6" )

# Import target "SimTKmath" for configuration "RelWithDebInfo"
set_property(TARGET SimTKmath APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(SimTKmath PROPERTIES
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "SimTKcommon;/usr/lib/libopenblas.so;/usr/lib/liblapack.so;/usr/lib/libopenblas.so;pthread;rt;dl;m"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib64/libSimTKmath.so.3.6"
  IMPORTED_SONAME_RELWITHDEBINFO "libSimTKmath.so.3.6"
  )

list(APPEND _IMPORT_CHECK_TARGETS SimTKmath )
list(APPEND _IMPORT_CHECK_FILES_FOR_SimTKmath "${_IMPORT_PREFIX}/lib64/libSimTKmath.so.3.6" )

# Import target "SimTKsimbody" for configuration "RelWithDebInfo"
set_property(TARGET SimTKsimbody APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(SimTKsimbody PROPERTIES
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELWITHDEBINFO "SimTKmath;SimTKcommon;/usr/lib/libopenblas.so;/usr/lib/liblapack.so;/usr/lib/libopenblas.so;pthread;rt;dl;m"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib64/libSimTKsimbody.so.3.6"
  IMPORTED_SONAME_RELWITHDEBINFO "libSimTKsimbody.so.3.6"
  )

list(APPEND _IMPORT_CHECK_TARGETS SimTKsimbody )
list(APPEND _IMPORT_CHECK_FILES_FOR_SimTKsimbody "${_IMPORT_PREFIX}/lib64/libSimTKsimbody.so.3.6" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
