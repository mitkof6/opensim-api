/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "API", "index.html", [
    [ "OpenSim 3.3 Documentation", "index.html", null ],
    [ "OpenSim Copyright and License", "OpenSim_license_page.html", null ],
    [ "Bug List", "bug.html", null ],
    [ "Namespace Members", "namespacemembers.html", [
      [ "All", "namespacemembers.html", null ],
      [ "Functions", "namespacemembers_func.html", null ],
      [ "Variables", "namespacemembers_vars.html", null ],
      [ "Typedefs", "namespacemembers_type.html", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List ", "annotated.html", "annotated_dup" ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"OpenSim_license_page.html",
"classOpenSim_1_1Actuation.html#acdf64e5af1a9b8db417a5d4e29f7e943",
"classOpenSim_1_1ArrowGeometry.html#ac6995bc156b9d4ab974ae319b990293a",
"classOpenSim_1_1CMC.html#a8f4e01522d63513797fbe49901399a6b",
"classOpenSim_1_1ClutchedPathSpring.html#ae4a9efacc26fbd076f438150254db867",
"classOpenSim_1_1Control.html#a5d50396bb91fd7b10757bb1576a7c292",
"classOpenSim_1_1Coordinate.html#aa56fa9e54e1f4ef67513027fd0c5f78d",
"classOpenSim_1_1DynamicsTool.html#a01b8561e3e01d3c8e9a5da7d59e1deb7",
"classOpenSim_1_1FiberCompressiveForceCosPennationCurve.html#a09772612afacfe43e715f30bd87c707a",
"classOpenSim_1_1FunctionBasedBushingForce.html#a13ae0c01a45ba0ad96f0d89f5b2f0f5e",
"classOpenSim_1_1IKMarkerTask.html#a2134ca90d2457521a06150ea93908a1f",
"classOpenSim_1_1JointReaction.html#ae62d72d48ba1c06706cfda3b1176f7a1",
"classOpenSim_1_1MarkerPlacer.html#a8ac429398096fe2e33c904025657c05b",
"classOpenSim_1_1Model.html#a2584ecdaac80a9db34e426549851972d",
"classOpenSim_1_1ModelScaler.html#ac99effe6ad5aeb464ea5e385ef653fe3",
"classOpenSim_1_1MuscleAnalysis.html#aa9496cbb045b2650c5aa006d44fb4f43",
"classOpenSim_1_1PathSpring.html#a11c14c6ad8a28e7c97d42b211e0f509d",
"classOpenSim_1_1PolynomialFunction.html#ae1eebfe9841559d0de9b0187faf99ebc",
"classOpenSim_1_1PropertyObj.html#a5b81fdcbb925e66c4ec0b895e430a8e5",
"classOpenSim_1_1Property__Deprecated.html#ae4a030614ef46e5ee403b3af4116d9d6",
"classOpenSim_1_1SimbodyEngine.html#a2a503f076e2207c67510552f1523e754",
"classOpenSim_1_1StaticOptimization.html#ad36218f1b9da8217690ad5a35b946a7f",
"classOpenSim_1_1Thelen2003Muscle.html#a16752248f106ab71a12bd33e3a1f93af",
"classOpenSim_1_1UnilateralConstraint.html#a610bdf80049872188d982d84b7002ee3",
"functions_o.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';