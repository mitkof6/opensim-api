var classOpenSim_1_1PolynomialFunction =
[
    [ "PolynomialFunction", "classOpenSim_1_1PolynomialFunction.html#a1fd667fb1182d85a23f4ab0a38f6e0f5", null ],
    [ "PolynomialFunction", "classOpenSim_1_1PolynomialFunction.html#abe2c15e870f0eac8632f03efe4118bc8", null ],
    [ "~PolynomialFunction", "classOpenSim_1_1PolynomialFunction.html#a1dded413aaa844b747706341cf4c837f", null ],
    [ "createSimTKFunction", "classOpenSim_1_1PolynomialFunction.html#ae1eebfe9841559d0de9b0187faf99ebc", null ],
    [ "getCoefficients", "classOpenSim_1_1PolynomialFunction.html#ad3f9c2c71ad9d286913499e7500cf38e", null ],
    [ "setCoefficients", "classOpenSim_1_1PolynomialFunction.html#aa0a065989de08ec7a0a5fa8a7dc92a41", null ]
];