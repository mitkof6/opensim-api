<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>OpenSim Documentation : How RRA Works</title>
        <link rel="stylesheet" href="styles/site.css" type="text/css" />
        <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>

    <body>
        <div id="page">
            <div id="main">
                <div id="main-header" class="pageSectionHeader">
                    <h1 id="title-heading" class="pagetitle">
                                                <span id="title-text">
                            OpenSim Documentation : How RRA Works
                        </span>
                    </h1>

                    <div class="page-metadata">
                        <p>This page last changed on Mar 20, 2013 by <font color="#0050B2">tkuchida</font>.</p>
                    </div>
                </div>

                <div id="content" class="view">
                    <div id="main-content" class="wiki-content group">
                    <p>The topics covered in this section include:</p><p><div>
<ul>
    <li><a href='#HowRRAWorks-Overview'>Overview</a></li>
    <li><a href='#HowRRAWorks-Toc174781415ResidualReductionAlgorithmRRA'><span class="confluence-anchor-link" id="HowRRAWorks-Toc174781415" />Residual Reduction Algorithm (RRA)</a></li>
<ul>
    <li><a href='#HowRRAWorks-Ref205284370TrackingSimulation'><span class="confluence-anchor-link" id="HowRRAWorks-Ref205284370" />Tracking Simulation</a></li>
    <li><a href='#HowRRAWorks-MassCenterAdjustment'>Mass Center Adjustment</a></li>
    <li><a href='#HowRRAWorks-MassAdjustmentRecommendation'>Mass Adjustment Recommendation</a></li>
    <li><a href='#HowRRAWorks-AdjustedKinematics'>Adjusted Kinematics</a></li>
</ul>
</ul></div></p><h1 id="HowRRAWorks-Overview">Overview</h1><p>Residual reduction is a form of forward dynamics simulation that uses a tracking controller to follow model kinematics determined from the inverse kinematics. Computed muscle control (CMC) serves as the controller, but without muscles the skeleton of the model can be used to determine a mass distribution and joint kinematics that are more consistent with ground reaction forces.</p><p>Residual reduction is primarily intended for gait, i.e., movements like walking and running where the model is displaced relative to the ground while subject to ground reaction forces and torques. In this chapter, we describe an example gait model (gait2354_simbody.osim) consisting of ten rigid segments (bones) where 17 of the 23 generalized coordinates (degrees of freedom) of the model represent angles for the joints connecting the rigid segments together. Each of these 17 degrees of freedom is actuated by a single torque actuator.</p><p>The remaining 6 generalized coordinates represent the 6 degrees of freedom (3 translational, 3 rotational) between the model's pelvis and the ground. To simulate walking, we need some way of representing how the model propels itself forward relative to the ground. One way would be to use a foot-ground contact mechanism.</p><p>Instead, we present a simpler solution: represent the 6 degrees of freedom between the pelvis and the ground as a 6-degree-of-freedom joint between the pelvis and the ground, and actuate each degree of fredom with its own torque actuator. Each of these 6 actuators is called a <span style="color: rgb(128,0,0);"><em>residual</em></span> <span style="color: rgb(128,0,0);"><em>actuator</em></span>. Now our model has 23 degrees of freedom and 23 actuators, i.e., exactly one actuator per degree of freedom. The three residuals that actuate the 3 translational degrees of freedom between the pelvis and the ground are the <span style="color: rgb(128,0,0);"><em>residual forces</em></span>, whose values we denote by <em>F<sub>x</sub></em>, <em>F<sub>y</sub></em>, and <em>F<sub>z</sub></em>. The 3 rotational degrees of freedom are actuated by the <span style="color: rgb(128,0,0);"><em>residual torques</em></span> (or <span style="color: rgb(128,0,0);"><em>moments</em></span>), whose values we denote by <em>M<sub>x</sub></em>, <em>M<sub>y</sub></em>, and <em>M<sub>z</sub></em>. <em>F<sub>x</sub></em> is the force applied along the X (forward) axis, <em>F<sub>y</sub></em> is the force applied along the Y (vertical) axis, <em>M<sub>x</sub></em> is the torque applied about the X (forward axis), and so on.</p><p>Typically, modeling assumptions (e.g., having a model with no arms), noise, and other errors from motion capture data lead to <span style="color: rgb(128,0,0);"><em>dynamic inconsistency</em></span>; essentially, the ground reaction forces and acceleration estimated from measured marker kinematics for a subject do not satisfy Newton's Second Law,
 
 
<style>
    #math1802887622 .typeset {
        color: black
    }
    #math1802887622 {
        font-size: 150%;
        margin: 1em 0px;
    }
    #jsMath_button, #jsMath_message, #jsMath_Warning {
        display: none;
    }
</style>
 
<script type="text/javascript" src="/includes/js/jsMath/plugins/noImageFonts.js">//<![CDATA[


//]]>
</script>
<script type="text/javascript" src="/includes/js/jsMath/jsMath.js">//<![CDATA[


//]]>
</script>
 
<span id="math1802887622" class="math">
  F = ma
</span>
 
<script type="text/javascript">//<![CDATA[

//check for use of this macro elsewhere on the page so that we only call jsMath once
 if (typeof mathFormulaDetected == 'undefined') {
    var mathFormulaDetected = false;
}
jQuery(document).ready(function() {
    if (!mathFormulaDetected) {
        jsMath.ProcessBeforeShowing();
        mathFormulaDetected = true;
    }
});

//]]>
</script>. Roughly speaking, the 6 residuals amount to adding a new force to the equation that accounts for inconsistency:</p>
 
 
<style>
    #math534249154 .typeset {
        color: black
    }
    #math534249154 {
        font-size: 150%;
        margin: 1em 0px;
    }
    #jsMath_button, #jsMath_message, #jsMath_Warning {
        display: none;
    }
</style>
 
<script type="text/javascript" src="/includes/js/jsMath/plugins/noImageFonts.js">//<![CDATA[


//]]>
</script>
<script type="text/javascript" src="/includes/js/jsMath/jsMath.js">//<![CDATA[


//]]>
</script>
 
<span id="math534249154" class="math">
  F + F_{\mathit{residual}} = ma
</span>
 
<script type="text/javascript">//<![CDATA[

//check for use of this macro elsewhere on the page so that we only call jsMath once
 if (typeof mathFormulaDetected == 'undefined') {
    var mathFormulaDetected = false;
}
jQuery(document).ready(function() {
    if (!mathFormulaDetected) {
        jsMath.ProcessBeforeShowing();
        mathFormulaDetected = true;
    }
});

//]]>
</script><h1 id="HowRRAWorks-Toc174781415ResidualReductionAlgorithmRRA"><span class="confluence-anchor-link" id="HowRRAWorks-Toc174781415"></span>Residual Reduction Algorithm (RRA)</h1><h2 id="HowRRAWorks-Ref205284370TrackingSimulation"><span class="confluence-anchor-link" id="HowRRAWorks-Ref205284370"></span>Tracking Simulation</h2><p>RRA begins by placing the model in the starting configuration, i.e., by setting the values of the model's generalized coordinates to the values computed by the inverse kinematics (IK) tool for the user-specified initial time (specified in the setup file as the <span style="color: rgb(0,0,255);">&lt;</span><span style="color: rgb(128,0,0);">initial_time</span><span style="color: rgb(0,0,255);">&gt;</span> property). Repeatedly, RRA takes small steps forward in time (with each time step of .001) until the user-specified final time (specified under the <span style="color: rgb(0,0,128);">&lt;final_time&gt;</span> property of the setup file) is reached. In each step, force values are computed for all of the model's actuators to make the model move from its current configuration to the configuration (generalized coordinates) desired at the end of the step, which is computed from the IK output. The actuator forces are computed by choosing force and torque values that minimize an objective function (see <a href="Settings%2BFiles%2Band%2BXML%2BTag%2BDefinitions.html">Settings Files and XML Tag Definitions</a> on Optimization Parameters).</p><h2 id="HowRRAWorks-MassCenterAdjustment">Mass Center Adjustment</h2><p>At the end of the simulation, the average value for each residual actuator is computed. The average values for <em>M<sub>x</sub></em> (the left-right residual torque) and <em>M<sub>z</sub></em> (the fore-aft residual torque) are used to adjust the torso mass center to correct excessive &quot;leaning&quot; of the model due to inaccuracies in the mass distribution and geometry of the torso in the model. A new model file containing the adjusted torso mass center (specified in the setup file under the <span style="color: rgb(0,0,128);">&lt;output_model_file&gt;</span> property) is created.</p><h2 id="HowRRAWorks-MassAdjustmentRecommendation">Mass Adjustment Recommendation</h2><p>The average value of <em>F<sub>y</sub></em> is used to compute the recommended mass changes for all of the body segments. The desired mass change is:</p>
 
 
<style>
    #math407780133 .typeset {
        color: black
    }
    #math407780133 {
        font-size: 150%;
        margin: 1em 0px;
    }
    #jsMath_button, #jsMath_message, #jsMath_Warning {
        display: none;
    }
</style>
 
<script type="text/javascript" src="/includes/js/jsMath/plugins/noImageFonts.js">//<![CDATA[


//]]>
</script>
<script type="text/javascript" src="/includes/js/jsMath/jsMath.js">//<![CDATA[


//]]>
</script>
 
<span id="math407780133" class="math">
  F_y\,/g
</span>
 
<script type="text/javascript">//<![CDATA[

//check for use of this macro elsewhere on the page so that we only call jsMath once
 if (typeof mathFormulaDetected == 'undefined') {
    var mathFormulaDetected = false;
}
jQuery(document).ready(function() {
    if (!mathFormulaDetected) {
        jsMath.ProcessBeforeShowing();
        mathFormulaDetected = true;
    }
});

//]]>
</script><p>where <em>g</em> = -9.80665 m/s<sup>2</sup>. This mass change is then divided up proportionally among the body segments. The computed mass changes are recommended to the user, who can make these changes in the OpenSim model file by hand. The recommended mass changes are NOT applied to the model automatically.</p><h2 id="HowRRAWorks-AdjustedKinematics">Adjusted Kinematics</h2><p>The same tracking simulation process is then repeated with three important differences:</p><ul><li>The model with the adjusted torso mass center is used</li><li>The residuals are weighted more heavily to make the optimizer choose smaller values for the residuals when minimizing the objective function</li><li>minimum and maximum limits are placed on the residual values</li></ul><p><br class="atl-forced-newline" /> The goal of these restrictions on the residual values is to reduce the need for residuals to the absolute minimum that is necessary to closely follow the desired kinematics so that the motion is generated purely by internal joint moments. During computed muscle control (CMC), the next stage of OpenSim, the moments will be generated by forces exerted by muscles. This way, biomechanical results about muscle function concluded from CMC will be closer to reality than if we let the residuals be arbitrarily large.</p><p>With these restrictions placed on the residuals, the model's motion will likely be altered since the residuals may not be allowed to reach the magnitudes that would result from inverse dynamics while following the kinematics from IK exactly. If the minimum and/or maximum allowed residual values are too restrictive, the motion will be altered so dramatically that the results of RRA cannot be used to generate a realistic simulation with CMC. If the residual minimum and/or maximum values are too lenient, then the residuals will still be large enough to exert forces that might normally be exerted by muscles, and thus the results would lead to unrealistic muscle function from CMC.</p><p> </p><div class="panel" style="background-color: white;border-color: gray;border-style: solid;border-width: 5px;"><div class="panelContent" style="background-color: white;">
<p>Next: <a href="How%2Bto%2BUse%2Bthe%2BRRA%2BTool.html">How to Use the RRA Tool</a></p><p>Previous: <a href="Getting%2BStarted%2Bwith%2BRRA.html">Getting Started with RRA</a></p><p>Home: <a href="Residual%2BReduction%2BAlgorithm.html">Residual Reduction Algorithm</a></p>
</div></div>
                    </div>

                                        <div class="pageSection group">
                        <div class="pageSectionHeader">
                            <h2 id="attachments" class="pageSectionTitle">Attachments:</h2>
                        </div>

                        <div class="greybox" align="left">
                                                            <img src="images/icons/bullet_blue.gif" height="8" width="8" alt=""/>
                                <a href="attachments/3376193/3737149.png">RRA1.png</a> (image/png)
                                <br/>
                                                            <img src="images/icons/bullet_blue.gif" height="8" width="8" alt=""/>
                                <a href="attachments/3376193/3737148.png">RRA2.png</a> (image/png)
                                <br/>
                                                            <img src="images/icons/bullet_blue.gif" height="8" width="8" alt=""/>
                                <a href="attachments/3376193/3737147.png">RRA3.png</a> (image/png)
                                <br/>
                                                            <img src="images/icons/bullet_blue.gif" height="8" width="8" alt=""/>
                                <a href="attachments/3376193/3737146.docx">RRA Equations.docx</a> (application/vnd.openxmlformats-officedocument.wordprocessingml.document)
                                <br/>
                                                    </div>
                    </div>
                    
                                                      
                </div>             </div> 
            <div id="footer" style="background: url(http://simtk-confluence.stanford.edu:8080/images/border/border_bottom.gif) repeat-x;">
                <p><small>Document generated by Confluence on Feb 24, 2014 09:27</small></p>
            </div>
        </div>     </body>
</html>
