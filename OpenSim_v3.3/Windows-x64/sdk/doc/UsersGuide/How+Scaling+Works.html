<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>OpenSim Documentation : How Scaling Works</title>
        <link rel="stylesheet" href="styles/site.css" type="text/css" />
        <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>

    <body>
        <div id="page">
            <div id="main">
                <div id="main-header" class="pageSectionHeader">
                    <h1 id="title-heading" class="pagetitle">
                                                <span id="title-text">
                            OpenSim Documentation : How Scaling Works
                        </span>
                    </h1>

                    <div class="page-metadata">
                        <p>This page last changed on Sep 25, 2012 by <font color="#0050B2">jenhicks</font>.</p>
                    </div>
                </div>

                <div id="content" class="view">
                    <div id="main-content" class="wiki-content group">
                    <p>The topics covered in this section include:</p><p><div>
<ul>
    <li><a href='#HowScalingWorks-Overview'>Overview</a></li>
    <li><a href='#HowScalingWorks-Scaling'>Scaling</a></li>
<ul>
    <li><a href='#HowScalingWorks-Measurement-basedScaling'>Measurement-based Scaling</a></li>
    <li><a href='#HowScalingWorks-ManualScaling'>Manual Scaling</a></li>
    <li><a href='#HowScalingWorks-MassScaling'>Mass Scaling</a></li>
</ul>
    <li><a href='#HowScalingWorks-MarkerPlacement'>Marker Placement</a></li>
</ul></div></p><h2 id="HowScalingWorks-Overview">Overview</h2><p>Scaling is performed based on a combination of measured distances between x-y-z marker locations and manually-specified scale factors. The marker locations are usually obtained using motion capture equipment. The unscaled model has a set of virtual markers placed in the same anatomical locations as the experimental markers.</p><p>The dimensions of each segment in the model are scaled so that the distances between the virtual markers match the distances between the experimental markers. Manual scale factors, which may come from other anthropometric analyses, can also be used as an alternative to the measurement-based scaling for any body segment. Once the dimensions of the segments have been scaled to match the subject, the Scale Tool can be used to move some or all of the virtual markers on the model so that they coincide with the experimental marker locations.</p><p><img class="confluence-embedded-image" height="516" width="401" src="attachments/3376178/3737048.png" data-image-src="attachments/3376178/3737048.png" title="worddavf9db5241d24a69a8bd785961cbfd5d3f.png [imported from a Word document]"><br /><strong>Experimental and Virtual Markers.</strong> Experimental marker positions are measured with motion capture equipment (<span style="color: rgb(0,0,128);">dark blue</span>). Virtual markers are placed on a model in anatomical correspondence (e.g., over the right anterior superior iliac spine (R.ASIS)). Distances between experimental markers (<span style="color: rgb(0,0,128);">e<sub>i</sub></span>) relative to the distances between corresponding virtual markers (<span style="color: rgb(255,0,255);">m<sub>i</sub></span>) are used to compute scale factors.</p><h2 id="HowScalingWorks-Scaling">Scaling</h2><p>The scaling step scales both the mass properties (mass and inertia tensor), as well as the dimensions of the body segments. The main task involved in this step is computing the scale factors for each body segment. This is accomplished using a combination of measurement-based and manual scaling.</p><h3 id="HowScalingWorks-Measurement-basedScaling">Measurement-based Scaling</h3><p>In measurement-based scaling, scale factors are determined by comparing distances between markers on the model and experimental marker positions provided in a .trc file (see <a href="Marker%2B%2528.trc%2529%2BFiles.html">Marker (.trc) Files</a> for more information about .trc files). A single scale factor is computed using one or more marker pairs.</p><p>For example, suppose two marker pairs are used: p<sub>1</sub>={R.ASIS, R.Knee.Lat} and p<sub>2</sub>={L.ASIS, L.Knee.Lat}. The distance for pair 1 on the model (m<sub>1</sub>) is computed by placing the model in its default configuration (all joint angles get assigned their default values, as specified in the <span style="color: rgb(0,0,128);">&lt;default_value&gt;</span> property of <span style="color: rgb(0,0,128);">&lt;Coordinate&gt;</span> in the OpenSim model (.osim) file). The experimental distance between pair 1 (e<sub>1</sub>) is computed by looking at each frame of experimental marker data in the given .trc file, computing the distance between the pair for that frame, and taking the average across all frames in a user-specified time range. The scale factor due to pair 1 is then s<sub>1</sub>=e<sub>1</sub>/m<sub>1</sub>. If the markers were further apart in the .trc file than on the model, this indicates that the segment(s) in the model supporting these markers are too small. The overall scale factor is then the average of the scale factors computed due to all of the pairs (e.g., s=(s<sub>1</sub>+s<sub>2</sub>)/2 in this case, where s<sub>2</sub> is the scale factor due to pair 2). This overall scale factor s can then be used to scale any segments, and along any combination of the X, Y, and Z axes.</p><h3 id="HowScalingWorks-ManualScaling">Manual Scaling</h3><p>As an alternative to computing scale factors using measured marker positions, it is possible to specify the x-y-z scale factors for a segment manually. This is useful if the actual scale factors for segments are known, or were computed using some alternative algorithm.</p><h3 id="HowScalingWorks-MassScaling">Mass Scaling</h3><p>The scale factors computed using measurement-based or manual scaling are used to scale the sizes of the segments. In addition, the masses of the segments are adjusted so that the total mass of the body equals the specified subject mass.</p><p>There are two different ways the individual segment masses may be adjusted. One approach is to <em>preserve the mass distribution,</em> which ensures that the masses of the subject-specific model segments are in the same proportion as they were in the generic model. This scales the masses using a constant factor independent of the scale factors that were used to scale the individual segment sizes. The alternative approach incorporates the size scale factors, still ensuring the total mass equals the subject mass, but having the mass of the scaled segments reflect their scale in size.</p><p>In any case, the inertia tensor of each segment is updated to reflect its new size and mass.</p><h2 id="HowScalingWorks-MarkerPlacement">Marker Placement</h2><p>After scaling the model, the next step is to move the model's markers to match experimental marker locations in a static pose. The static pose is computed by trying to match some combination of experimental marker positions and generalized coordinate values, as in the inverse kinematics (IK) step (<a href="How%2BInverse%2BKinematics%2BWorks.html">How Inverse Kinematics Works</a>). The marker locations corresponding to the static pose are computed by averaging the marker positions in a given .trc file across a user-specified time range. Just like IK, marker and coordinate weights are used to determine how strongly the algorithm should try to match them. Once a static pose is computed using the IK-based algorithm, all model markers (except for those designated as fixed) are moved to the averaged &quot;static pose&quot; positions of the experimental markers.</p><p> </p><div class="panel" style="background-color: white;border-color: gray;border-style: solid;border-width: 5px;"><div class="panelContent" style="background-color: white;">
<p>Next: <a href="How%2Bto%2BUse%2Bthe%2BScale%2BTool.html">How to Use the Scale Tool</a></p><p>Previous: <a href="Getting%2BStarted%2Bwith%2BScaling.html">Getting Started with Scaling</a></p><p>Home: <a href="Scaling.html">Scaling</a></p>
</div></div>
                    </div>

                                        <div class="pageSection group">
                        <div class="pageSectionHeader">
                            <h2 id="attachments" class="pageSectionTitle">Attachments:</h2>
                        </div>

                        <div class="greybox" align="left">
                                                            <img src="images/icons/bullet_blue.gif" height="8" width="8" alt=""/>
                                <a href="attachments/3376178/3737048.png">worddavf9db5241d24a69a8bd785961cbfd5d3f.png</a> (image/png)
                                <br/>
                                                    </div>
                    </div>
                    
                                                      
                </div>             </div> 
            <div id="footer" style="background: url(http://simtk-confluence.stanford.edu:8080/images/border/border_bottom.gif) repeat-x;">
                <p><small>Document generated by Confluence on Feb 24, 2014 09:27</small></p>
            </div>
        </div>     </body>
</html>
