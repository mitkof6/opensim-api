<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>OpenSim Documentation : Previewing Motion Capture (Mocap) Data</title>
        <link rel="stylesheet" href="styles/site.css" type="text/css" />
        <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>

    <body>
        <div id="page">
            <div id="main">
                <div id="main-header" class="pageSectionHeader">
                    <h1 id="title-heading" class="pagetitle">
                                                <span id="title-text">
                            OpenSim Documentation : Previewing Motion Capture (Mocap) Data
                        </span>
                    </h1>

                    <div class="page-metadata">
                        <p>This page last changed on Jun 10, 2013 by <font color="#0050B2">jenhicks</font>.</p>
                    </div>
                </div>

                <div id="content" class="view">
                    <div id="main-content" class="wiki-content group">
                    <p>Motion capture data can be previewed in OpenSim to verify that preprocessing was done correctly and that data is in agreement with the intended model. See <a href="Preparing%2BYour%2BData.html">Preparing Your Data</a> for more information about how to import your data into OpenSim. <span style="font-size: 10.0pt;line-height: 13.0pt;">In some cases, you can also use OpenSim's feature for </span><a href="Associating%2BData%2Bwith%2Ba%2BMotion.html">Associating Data with a Motion</a><span style="font-size: 10.0pt;line-height: 13.0pt;">. For example, if you've already run IK and you want to visualize marker data or ground reaction forces in sync with the inverse kinematics trial you should use the Associate Data motion feature.</span></p><p>If you have multiple files representing different pieces of data, this tool allows you to verify that the data was transformed consistently. Since the goal is to ensure that the data is consistent with an OpenSim model for further analysis, this module handles data files in the format accepted by OpenSim tools. The two types of data currently handled are marker trajectories (contained in <span style="color: rgb(128,0,0);"><em>.trc</em></span> files) and measured forces (contained in <span style="color: rgb(128,0,0);"><em>.mot</em></span> files), described earlier.  The topics covered in this section include:</p><p><div>
<ul>
    <li><a href='#PreviewingMotionCaptureMocapData-UsingtheDataPreviewer'>Using the Data Previewer</a></li>
    <li><a href='#PreviewingMotionCaptureMocapData-VisualizingMarkerTrajectoriesinOpenSim'>Visualizing Marker Trajectories in OpenSim</a></li>
    <li><a href='#PreviewingMotionCaptureMocapData-VisualizingExternalForcesinOpenSim'>Visualizing External Forces in OpenSim</a></li>
    <li><a href='#PreviewingMotionCaptureMocapData-PreviewingTransformedData'>Previewing Transformed Data</a></li>
</ul></div></p><h2 id="PreviewingMotionCaptureMocapData-UsingtheDataPreviewer">Using the Data Previewer</h2><p>To choose the motion capture data file to be visualized, select <strong>Preview Experimental</strong> <strong>Data…</strong> from the <strong>File</strong> menu (Preview Motion Data in 3.0.1 and earlier versions of OpenSim). Once selected, OpenSim performs the following actions:</p><ul><li>Adds a new model to the Navigator with the default name of &quot;ExperimentalData&quot; and a unique number so that multiple data files can be uniquely identified. The model has no display-offset in the GUI to be faithful to the data.</li><li>Makes the loaded motion capture data file the <em>current</em> motion.</li></ul><p><br class="atl-forced-newline" /> This enables you to perform the following tasks:</p><ul><li>Use the motion slider in the toolbar to go through different frames</li><li>Synchronize the motions from different motion files. These other motions could be either motion capture files or results from OpenSim tools. For example, marker trajectories and ground reaction forces can be synchronized and superimposed on the result of a forward simulation.</li></ul><h2 id="PreviewingMotionCaptureMocapData-VisualizingMarkerTrajectoriesinOpenSim">Visualizing Marker Trajectories in OpenSim</h2><p>Marker trajectories are expected to be contained within a <span style="color: rgb(128,0,0);"><em>.trc</em></span> file. The ExperimentalData model will contain the loaded motion file (e.g., subject01_walk1.trc) with marker data. The <strong>Markers</strong> node will list each individual marker found in the file (figure below). Nodes corresponding to individual markers have the following options, accessible by right-clicking the mouse:</p><ul><li><strong>Show</strong>: Enabled only if a marker is hidden</li><li><strong>Show Only</strong>: Hides all other markers except for those selected</li><li><strong>Toggle Trail Display</strong>: Toggles the display of a line representation of the trajectory of selected marker(s)</li></ul><p><br class="atl-forced-newline" /> <img class="confluence-embedded-image" width="300" src="attachments/3376142/3736899.png" data-image-src="attachments/3376142/3736899.png"><br /><strong>Navigator View of a Trajectory Data File</strong><br /><br class="atl-forced-newline" /></p><h2 id="PreviewingMotionCaptureMocapData-VisualizingExternalForcesinOpenSim">Visualizing External Forces in OpenSim</h2><p>External forces (such as ground reaction forces) in OpenSim are contained in a <span style="color: rgb(128,0,0);"><em>.mot</em></span> file. The header of the selected force (<span style="color: rgb(128,0,0);"><em>.mot</em></span>) file is the same as that expected by OpenSim tools (<a href="Motion%2B%2528.mot%2529%2BFiles.html">Motion (.mot) Files</a>).</p><p>For the ground reaction forces used by the gait model, the column labels are shown below (for 2 external forces force1, force2, torque1, torque2):</p><ul><li>ground_force1_vx, ground_force1_vy, ground_force1_vz,</li><li>ground_force1_px, ground_force1_py, ground_force1_pz,</li><li>ground_force2_vx, ground_force2_vy, ground_force2_vz,</li><li>ground_force2_px, ground_force2_py, ground_force2_pz,</li><li>ground_torque1_x,  ground_torque1_y, ground_torque1_z,</li><li>ground_torque2_x, ground_torque2_y, ground_torque2_z</li></ul><p>The data previewer expects groups of 6 columns for a force of the form “body”_”ForceName”_{vx, vy, vz, px, py, pz}, where <em>vi</em> corresponds to each component of the force and <em>pi</em> corresponds to each component of the location of the force (e.g., center of pressure) and 3 columns for a torque of the form “body”_”TorqueName”_{x, y, z}, corresponding to each component of the applied torque. Note that this naming convention is only necessary for previewing purposes. OpenSim tools employ a new user interface enabling you to specifiy any number of forces, along with points of application, or torques to a model during any simulation or analysis.</p><h2 id="PreviewingMotionCaptureMocapData-PreviewingTransformedData">Previewing Transformed Data</h2><p>For any kind of previewed data (e.g., marker trajectories or ground reaction forces), you can visualize the effect of a rigid-body-transform applied to the data and save the data as a new file. </p><div class='panelMacro'><table class='noteMacro'><colgroup><col width='24'><col></colgroup><tr><td valign='top'><img src="images/icons/emoticons/warning.png" width="16" height="16" align="absmiddle" alt="" border="0"></td><td><p>OpenSim does not modify the data contained in the file.</p></td></tr></table></div><p>However, the visualization is transformed to enable you to modify the preprocessing tools, as needed. To access the visual transformation dialog window: </p><div class="table-wrap"><table class="confluenceTable"><tbody><tr><th class="nohighlight confluenceTh"><img class="confluence-embedded-image" src="attachments/3376142/3736898.png" data-image-src="attachments/3376142/3736898.png"></th><th class="nohighlight confluenceTh"><ul><li><strong>Right-mouse click</strong><span> on the navigator node corresponding to the motion file viewed (e.g. subject_01_walk1.trc in the example above). Choose <strong>Transform </strong>from the drop-down menu.</span></li><li><strong>Double click </strong>on any listed folders to display the list of markers (e.g. Lab Frame in the example at right).</li><li>Use the arrow buttons to change the angle for <strong>Rotate X</strong>, <strong>Rotate Y</strong>, and <strong>Rotate Z</strong>; this will transform the data in the viewer (and in the viewer only – not the data file itself). </li><li>Angles are always positive (0-360 degrees). </li><li>Translations can be previewed by modifying the display offset. </li></ul><div class='panelMacro'><table class='infoMacro'><colgroup><col width='24'><col></colgroup><tr><td valign='top'><img src="images/icons/emoticons/information.png" width="16" height="16" align="absmiddle" alt="" border="0"></td><td><p>The data file itself is NOT modified by OpenSim; the data previewer just displays the transformed data, so that the appropriate transformation(s) may be determined and applied during preprocessing.</p></td></tr></table></div><p><span><br /></span></p></th></tr><tr><td class="confluenceTd"><img class="confluence-embedded-image" width="500" src="attachments/3376142/3736897.png" data-image-src="attachments/3376142/3736897.png"></td><td class="confluenceTd"><ul><li>Select the marker data and grf data to sync the data and visualize the ground reaction forces with the marker motions.</li><li>See <a href="Loading%2BMotions.html">Loading Motions</a> for more information about how to sync motions.</li></ul><p> </p></td></tr></tbody></table></div><p><strong>Transform Data Previewer Window, </strong><strong>Preview of Motion Capture Data in OpenSim</strong></p><p> </p><div class="panel" style="background-color: white;border-color: gray;border-style: solid;border-width: 5px;"><div class="panelContent" style="background-color: white;">
<p>Next: <a href="Importing%2Band%2BExporting%2BSIMM%2BModels.html">Importing and Exporting SIMM Models</a></p><p>Previous: <a href="Associating%2BData%2Bwith%2Ba%2BMotion.html">Associating Data with a Motion</a></p><p>Home: <a href="Preparing%2BYour%2BData.html">Preparing Your Data</a></p>
</div></div>
                    </div>

                                        <div class="pageSection group">
                        <div class="pageSectionHeader">
                            <h2 id="attachments" class="pageSectionTitle">Attachments:</h2>
                        </div>

                        <div class="greybox" align="left">
                                                            <img src="images/icons/bullet_blue.gif" height="8" width="8" alt=""/>
                                <a href="attachments/3376142/3736899.png">previewmotion-1.PNG</a> (image/png)
                                <br/>
                                                            <img src="images/icons/bullet_blue.gif" height="8" width="8" alt=""/>
                                <a href="attachments/3376142/3736898.png">previewmotion-2.PNG</a> (image/png)
                                <br/>
                                                            <img src="images/icons/bullet_blue.gif" height="8" width="8" alt=""/>
                                <a href="attachments/3376142/3736897.png">previewmotion-3.PNG</a> (image/png)
                                <br/>
                                                    </div>
                    </div>
                    
                                                      
                </div>             </div> 
            <div id="footer" style="background: url(http://simtk-confluence.stanford.edu:8080/images/border/border_bottom.gif) repeat-x;">
                <p><small>Document generated by Confluence on Feb 24, 2014 09:27</small></p>
            </div>
        </div>     </body>
</html>
