<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>OpenSim Documentation : Getting Started with Static Optimization</title>
        <link rel="stylesheet" href="styles/site.css" type="text/css" />
        <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>

    <body>
        <div id="page">
            <div id="main">
                <div id="main-header" class="pageSectionHeader">
                    <h1 id="title-heading" class="pagetitle">
                                                <span id="title-text">
                            OpenSim Documentation : Getting Started with Static Optimization
                        </span>
                    </h1>

                    <div class="page-metadata">
                        <p>This page last changed on Oct 04, 2012 by <font color="#0050B2">jenhicks</font>.</p>
                    </div>
                </div>

                <div id="content" class="view">
                    <div id="main-content" class="wiki-content group">
                    <div class="wiki-content" style="margin-left: 0.0px;"><p>Static optimization is an extension to inverse dynamics that further resolves the net joint moments into individual muscle forces at each instant in time. The muscle forces are resolved by minimizing the sum of squared (or other power) muscle activations.</p><p>To launch the Static Optimization Tool, select <strong>Static Optimization…</strong> from the <strong>Tools</strong> menu. The <em>Static Optimization Tool</em> dialog window, like all other OpenSim tools, operates on the current model open and selected in OpenSim.</p><p><div>
<ul>
    <li><a href='#GettingStartedwithStaticOptimization-Overview'>Overview</a></li>
    <li><a href='#GettingStartedwithStaticOptimization-Inputs'>Inputs</a></li>
    <li><a href='#GettingStartedwithStaticOptimization-Outputs'>Outputs</a></li>
    <li><a href='#GettingStartedwithStaticOptimization-BestPracticesandTroubleshooting'>Best Practices and Troubleshooting</a></li>
<ul>
    <li><a href='#GettingStartedwithStaticOptimization-StaticOptimizationSettings'>Static Optimization Settings:</a></li>
    <li><a href='#GettingStartedwithStaticOptimization-Troubleshooting'>Troubleshooting:</a></li>
    <li><a href='#GettingStartedwithStaticOptimization-EvaluatingyourResults'>Evaluating your Results:</a></li>
</ul>
</ul></div></p><h2 id="GettingStartedwithStaticOptimization-Overview">Overview</h2><p>The figure below shows the required inputs and outputs for the Static Optimization Tool. Each is described in more detail in the following sections.</p><p> </p></div><div class="wiki-content" style="margin-left: 0.0px;"><p><img class="confluence-embedded-image" src="attachments/3376184/3737092.png" data-image-src="attachments/3376184/3737092.png"></p><p><strong>Inputs and Outputs of the Static Optimization Tool.</strong> Experimental data are shown in <span style="color: rgb(0,204,153);">green</span>; OpenSim files (.osim) are shown in <span style="color: rgb(165,0,33);">red</span>; settings files are shown in <span style="color: rgb(51,102,255);">blue</span>; files generated by the workflow are shown in <span style="color: rgb(128,0,128);">purple</span>. To run static optimization, you use the analyze command.</p><div class='panelMacro'><table class='infoMacro'><colgroup><col width='24'><col></colgroup><tr><td valign='top'><img src="images/icons/emoticons/information.png" width="16" height="16" align="absmiddle" alt="" border="0"></td><td><p>The file names are examples that can be found in the examples/Gait2354_Simbody directory installed with the OpenSim distribution.</p></td></tr></table></div><h2 id="GettingStartedwithStaticOptimization-Inputs">Inputs</h2><p>Three files are required as input by the Static Optimization Tool:</p><p><span style="color: rgb(128,0,128);"><strong>subject01_walk1_ik.mot</strong></span>: Motion file containing the time histories of generalized coordinates that describe the movement of the model. This can be kinematic data (i.e., joint angles) from IK or states (i.e., joint angles AND velocities) from RRA for the time range of interest.</p><p><span style="color: rgb(0,204,153);"><strong>subject01_walk1_grf.xml</strong></span>: External load data (i.e., ground reaction forces, moments, and center of pressure location). Note that you must measure or model all external forces acting on a subject during the motion to calculate accurate muscle forces. The .xml file describes how to apply the measured ground reaction forces to the model during the analysis.</p><p><span style="color: rgb(128,0,0);"><strong>subject01_simbody.osim</strong></span>: A subject-specific OpenSim model generated by scaling a generic model with the Scale Tool or by other means, along with an associated marker set containing adjusted virtual markers. The model must include inertial parameters (segment masses, etc.).</p><p><strong><span style="color: rgb(0,0,255);">x</span>:</strong> The exponent for the activation-based cost function to be minimized (i.e., the criterion used to solve the muscle force distribution problem).</p><h2 id="GettingStartedwithStaticOptimization-Outputs">Outputs</h2><p>The Static Optimization Tool generates three files in a specified folder:</p><p><span style="color: rgb(128,0,128);"><strong>subject01_walk1_StaticOptimization_controls.xml</strong></span>: Contains the time histories of muscle activations. These controls were minimized by the Static Optimization Tool.</p><p><span style="color: rgb(128,0,128);"><strong>subject01_walk1_StaticOptimization_activation.sto</strong></span>: Storage file containing the time histories of muscle activations.</p><p><span style="color: rgb(128,0,128);"><strong>subject01_walk1_StaticOptimization_force.sto</strong></span>: Storage file containing the time histories of muscle forces.</p><h2 id="GettingStartedwithStaticOptimization-BestPracticesandTroubleshooting">Best Practices and Troubleshooting</h2><p><h4 id="GettingStartedwithStaticOptimization-StaticOptimizationSettings">Static Optimization Settings:</h4><ol><li>You can use IK or RRA results as input kinematics. If using IK results, you usually need to filter them, either externally or using the OpenSim analyze/static optimization field; if using RRA results, you usually do not have to filter.</li><li>For gait and many other motions, you need to add (append) residual actuators to the first free joint in the model (typically the ground-pelvis joint).<br /><ol><li>There should be one actuator for each degree-of-freedom (i.e., F<sub>X</sub>, F<sub>Y</sub>, F<sub>Z</sub>, M<sub>X</sub>, M<sub>Y</sub>, M<sub>Z</sub>).</li><li>These residual actuators are required because there is dynamic inconsistency between the estimated model accelerations and the measured ground reaction forces. This inconsistency can result from marker measurement error, differences between the geometry of the model and the subject, and inertial parameters.</li><li>Running RRA will reduce—but not eliminate—these residuals. Thus, appending actuators is still necessary.</li></ol></li><li>See <a href="http://simtk-confluence.stanford.edu:8080/display/OpenSim/How+Static+Optimization+Works">How Static Optimization Works</a> and <a href="http://simtk-confluence.stanford.edu:8080/display/OpenSim/How+to+Use+the+Static+Optimization+Tool">How to Use the Static Optimization Tool</a> for more information.</li></ol><h4 id="GettingStartedwithStaticOptimization-Troubleshooting">Troubleshooting:</h4><ol><li>If the residual actuators or the model's muscles are weak, the optimization will take a long time to converge or will never converge at all.<br /><ol><li>If the residual actuators are weak, increase the maximum control value of a residual, while lowering its maximum force. This allows the optimizer to generate a large force (if necessary) to match accelerations, but large control values are penalized more heavily. In static optimization, ideal actuator excitations are treated as activations in the cost function.</li><li>If the muscles are weak, <span>append Coordinate Actuators to the model at the joints in the model. This will allow you to see how much &quot;reserve&quot; actuation is required at a given joint and then strengthen the muscles in your model accordingly.</span></li><li>If troubleshooting a weak model and optimization is slow each time, try reducing the parameter that defines the maximum number of iterations.</li></ol></li><li><span>Static optimization works internally by solving the inverse dynamics problem, then trying to solve the redundancy problem for actuators/muscles using the accelerations from the inverse dynamics solution<span style="color: rgb(0,0,0);"> as a </span></span><span class="posthilit" style="color: rgb(0,0,0);">constraint. If a constraint violation is reported, this could be</span><span><span style="color: rgb(0,0,0);"> a sig</span>n that the optimizer couldn't solve for muscle forces while enforcing the inverse dynamics solution.</span><br /><ol><li><span>This likely means that there is noise in the data or there is a sudden jump in accelerations in one frame.</span></li><li><span>In this case, you should examine the inverse dynamics solution to determine the problematic frame, and fix/interpolate the data during this portion of the motion.</span></li></ol></li></ol><h4 id="GettingStartedwithStaticOptimization-EvaluatingyourResults">Evaluating your Results:</h4><ol><li>Are there any large or unexpected residual actuator forces?</li><li>Find EMG or muscle activation data for comparison with your simulated activations. Does the timing of muscle activation/deactivation match? Are the magnitudes and patterns in good agreement?</li></ol></p></div><div class="pageSection group"><p> </p><div class="panel" style="background-color: white;border-color: gray;border-style: solid;border-width: 5px;"><div class="panelContent" style="background-color: white;">
<p>Next: <a href="How%2BStatic%2BOptimization%2BWorks.html">How Static Optimization Works</a></p><p>Home: <a href="Static%2BOptimization.html">Static Optimization</a></p>
</div></div></div>
                    </div>

                                        <div class="pageSection group">
                        <div class="pageSectionHeader">
                            <h2 id="attachments" class="pageSectionTitle">Attachments:</h2>
                        </div>

                        <div class="greybox" align="left">
                                                            <img src="images/icons/bullet_blue.gif" height="8" width="8" alt=""/>
                                <a href="attachments/3376184/3737092.png">StaticToolInputOutputDiagrams.png</a> (image/png)
                                <br/>
                                                    </div>
                    </div>
                    
                                                      
                </div>             </div> 
            <div id="footer" style="background: url(http://simtk-confluence.stanford.edu:8080/images/border/border_bottom.gif) repeat-x;">
                <p><small>Document generated by Confluence on Feb 24, 2014 09:27</small></p>
            </div>
        </div>     </body>
</html>
