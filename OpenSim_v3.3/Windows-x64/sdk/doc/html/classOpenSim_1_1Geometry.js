var classOpenSim_1_1Geometry =
[
    [ "GeometryType", "classOpenSim_1_1Geometry.html#ae8156c775cbd2ef2bba59ea057492ab1", [
      [ "None", "classOpenSim_1_1Geometry.html#ae8156c775cbd2ef2bba59ea057492ab1a6c4abbb6534e767bd8e2812ab1845ea3", null ],
      [ "Sphere", "classOpenSim_1_1Geometry.html#ae8156c775cbd2ef2bba59ea057492ab1a1384fd25c2a7a4b72173f2d505681e58", null ],
      [ "Cylinder", "classOpenSim_1_1Geometry.html#ae8156c775cbd2ef2bba59ea057492ab1ad73ec6116855aae7b9a4b90556bc5cde", null ],
      [ "Cone", "classOpenSim_1_1Geometry.html#ae8156c775cbd2ef2bba59ea057492ab1a1080a1705c01c035f77913322e416995", null ],
      [ "Ellipsoid", "classOpenSim_1_1Geometry.html#ae8156c775cbd2ef2bba59ea057492ab1ab543c6f1424ae021d323b039f6b6c9a5", null ],
      [ "Torus", "classOpenSim_1_1Geometry.html#ae8156c775cbd2ef2bba59ea057492ab1a71f86ada6a001a54b6822e5de98ca8b3", null ],
      [ "Line", "classOpenSim_1_1Geometry.html#ae8156c775cbd2ef2bba59ea057492ab1a2f19879af1be76259bcc9c54337fe2e6", null ],
      [ "Arrow", "classOpenSim_1_1Geometry.html#ae8156c775cbd2ef2bba59ea057492ab1a88a98ed1163f8e63e727c3ca6a466100", null ]
    ] ],
    [ "Geometry", "classOpenSim_1_1Geometry.html#a79ab6403b824473d13c798c1ce6f9e26", null ],
    [ "~Geometry", "classOpenSim_1_1Geometry.html#a047a4cece89b71ea8130b9125e183680", null ],
    [ "getFixed", "classOpenSim_1_1Geometry.html#af2bb74fe70236a3b834116570050fe27", null ],
    [ "getShape", "classOpenSim_1_1Geometry.html#a322e50593cb75e42aeb394a4d830ffbd", null ],
    [ "isAnalytic", "classOpenSim_1_1Geometry.html#a2c3ac69ba3fcc1275dfba2ea9af2ffb6", null ],
    [ "setFixed", "classOpenSim_1_1Geometry.html#a9b3119599236cef7d20a2864234db5a1", null ],
    [ "_analyticType", "classOpenSim_1_1Geometry.html#acbb12bbbc18564608aa79bb9baf2cd3c", null ]
];